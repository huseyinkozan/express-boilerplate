# Express Boilerplate

My express boilerplate.

Usage:
* Clone:
    ```
    git clone ...
    ```
* Install PM2:
    ```
    npm i -g pm2
    ```
* Install Node modules:
    ```
    npm i
    ```
* Start server at [localhost:3000](http://localhost:3000/):
    ```
    pm2-dev start process.yml
    ```
* Develop ...
