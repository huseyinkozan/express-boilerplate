const createError = require("http-errors");
const express = require("express");
const path = require("path");
const helmet = require("helmet");
const {RateLimiterMemory} = require("rate-limiter-flexible");
const cookieParser = require("cookie-parser");
const compression = require("compression");
const logger = require("morgan");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");

let app = express();

app.use(helmet());

const rateLimiter = new RateLimiterMemory({points: 6, duration: 1});

app.use(function(req, res, next) {
  rateLimiter.consume(req.ip)
    .then(() => {
      next();
    })
    .catch(() => {
      res.status(429).send("Too Many Requests");
    });
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// Compression middleware (should be placed before express.static)
app.use(compression({
  threshold: 512
}));

app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, _next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
