Started with [Express installing guide](http://expressjs.com/en/starter/installing.html).
```
mkdir express-boilerplate
cd express-boilerplate
npm init
npm install express --save
```

Added `.gitignore` file using [gitignore.io/api/node](https://www.gitignore.io/api/node).

Added eslint. With Atom linter-eslint plugin, shows linter errors and warnings.

```
npm install eslint eslint-plugin-json eslint-plugin-node --save-dev
```

Created a project with express-generator and copy created contents.
```
cd /tmp
express --view=pug --git express-boilerplate
meld express-boilerplate /tmp/express-boilerplate
npm install cookie-parser debug http-errors morgan pug --save
```

Applied some express best practices;
[performance](http://expressjs.com/en/advanced/best-practice-performance.html),
[security](https://expressjs.com/en/advanced/best-practice-security.html).

```
npm install --save compression helmet rate-limiter-flexible
```
